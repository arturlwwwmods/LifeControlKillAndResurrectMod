import codecs
import compileall
import os
import zipfile

for root, dirs, files in os.walk("."):
    path = root.split(os.sep)

    for file in files:
        extension = os.path.splitext(file)[1]
        if extension in (".pyo", ".pyc", ".zip", ".ts4script"):
            oldPath = os.path.realpath(root) + os.sep + file
            os.remove(oldPath)

current_path = os.path.realpath(".")
buildFileName = current_path + os.sep + "build.py"
runBatFileName = current_path + os.sep + "runBat.py"
packageFileName = current_path + os.sep + "package.py"

for root, dirs, files in os.walk("."):
    p = os.path.realpath(root)

    p = p.replace(current_path + os.sep, "")
    p = p.replace(os.sep, ".")
    p_prev = p[0:(p.rfind("."))]

    for file in files:
        ext = os.path.splitext(file)[1]
        if ext == '.py':
            # print("FOUND PY FILE!!!"+file+" "+ext)
            oldPath = os.path.realpath(root) + os.sep + file
            if oldPath != buildFileName \
                    and oldPath != runBatFileName \
                    and oldPath != packageFileName:
                with codecs.open(oldPath, 'r+', 'utf-8') as myfile:
                    result = ""
                    for line in myfile:
                        data = line.replace('from . ', "from " + p + " ")
                        data = data.replace('from .. ', "from " + p_prev + " ")
                        data = data.replace('from ..', "from " + p_prev + ".")
                        data = data.replace('from .', "from " + p + ".")
                        result = result + data
                    myfile.seek(0)
                    myfile.write(result)
                    myfile.truncate()

compileall.compile_dir('.', 10, None, True, None, False, False)

for root, dirs, files in os.walk("."):
    path = root.split(os.sep)
    # print((len(path) - 1) * '---', os.path.basename(root), ' --- ', os.path.realpath(root))

    for file in files:
        # a = len(path) * '---'
        if os.path.basename(root).find("__pycache__") > -1:
            oldPath = os.path.realpath(root) + os.sep + file
            newPath = os.path.realpath(root).replace(os.sep + "__pycache__", "") + os.sep + file.replace(".cpython-33",
                                                                                                         "")
            os.rename(oldPath, newPath)
            # print(a,file)

zf = zipfile.ZipFile('MTS_ArtUrlWWW_LifeControlMod_Script.ts4script', mode='w')
try:
    for root, dirs, files in os.walk("."):
        path = root.split(os.sep)

        if root.find(".git") < 0 and root.find(".idea") < 0:

            for file in files:
                if file.find(".bat") < 0 and file.find(".zip") < 0 \
                        and file.find(".ts4script") < 0 \
                        and file.find("build.py") < 0 \
                        and file.find("build.pyo") < 0 \
                        and file.find("runBat.py") < 0 \
                        and file.find("runBat.pyo") < 0 \
                        and file.find("uncompiled_file.py") < 0 \
                        and file.find("uncompiled_file.pyo") < 0 \
                        and file.find("uncompile.py") < 0 \
                        and file.find("uncompile.pyo") < 0 \
                        and file.find("package.py") < 0 \
                        and file.find("package.pyo") < 0:
                    extension = os.path.splitext(file)[1]
                    # if extension in (".pyo", ".pyc"):
                    oldPath = os.path.realpath(root) + os.sep + file
                    oldPath = oldPath.replace(current_path + os.sep, "")
                    zf.write(oldPath)


finally:
    zf.close()
