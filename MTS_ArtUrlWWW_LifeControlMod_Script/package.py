import os

import glob
import shutil
import zipfile
from os.path import basename

from arturlwwwlifecontrolmod.VersionInfo import GLOBAL_VERSION, RELEASE_BUILD_NUMBER

modMainDir = 'c:/Users/artkh/OneDrive/Документы/Electronic Arts/The Sims 4/Mods/MTS_ArtUrlWWW_LifeControlMod/'

if not os.path.exists(modMainDir):
    os.makedirs(modMainDir)

for file in glob.glob(r'./*.ts4script'):
    print(file)
    shutil.copy(file, modMainDir)

for file in glob.glob(r'../*.package'):
    print(file)
    shutil.copy(file, modMainDir)

for file in glob.glob(modMainDir + '*.zip'):
    os.remove(file)

zf = zipfile.ZipFile(modMainDir + 'MTS_ArtUrlWWW_LifeControlMod_v.' + GLOBAL_VERSION + '.' + RELEASE_BUILD_NUMBER + '.zip',
                     mode='w',
                     compression=zipfile.ZIP_DEFLATED)
try:
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_LifeControlMod.package'
    zf.write(pathToFile, basename(pathToFile))
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_LifeControlMod_Script.ts4script'
    zf.write(pathToFile, basename(pathToFile))
finally:
    zf.close()
