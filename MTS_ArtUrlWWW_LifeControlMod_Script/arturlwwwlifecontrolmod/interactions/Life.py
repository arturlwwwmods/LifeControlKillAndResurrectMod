from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from objects import ALL_HIDDEN_REASONS
from sims.sim import Sim
from sims.sim_info import SimInfo

from arturlwwwlifecontrolmod.Life import revivesim, KillCurrentSim, \
    addmoneydialog
from arturlwwwlifecontrolmod.utils.exceptions_handler import exception_catcher


class SetAllCommoditiesToMax(ImmediateSuperInteraction):
    __qualname__ = 'SetAllCommoditiesToMax'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

        picked_sim.commodity_tracker.set_all_commodities_to_max(visible_only=True)
        picked_sim.singed = False

        picked_sim.commodity_tracker.update_all_commodities()


class PregnancyMegaModKillCurrentSim(ImmediateSuperInteraction):
    __qualname__ = 'PregnancyMegaModKillCurrentSim'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        KillCurrentSim(sim_info)


class PregnancyMegaModReviveSim(ImmediateSuperInteraction):
    __qualname__ = 'PregnancyMegaModReviveSim'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        revivesim()


class AddMoneyDialog(ImmediateSuperInteraction):
    __qualname__ = 'AddMoneyDialog'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

            addmoneydialog()
