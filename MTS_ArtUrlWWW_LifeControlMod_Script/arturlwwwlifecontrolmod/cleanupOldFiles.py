import os

this_file_path = os.path.realpath(__file__)
pos = this_file_path.find('ts4script') + 9
this_file_path = this_file_path[:pos]
this_file_time = os.path.getmtime(this_file_path)

pos = this_file_path.find('Mods') + 4
mods_dir_path = this_file_path[:pos]

# f.write(str(file_time2) + "\n")


for root, dirs, files in os.walk(mods_dir_path):

    for file in files:
        file_path = os.path.join(root, file)
        if 'ArtUrlWWW_LifeControlMod' in file_path:
            if ('zip' in file_path) or ('ts4script' in file_path):
                t = os.path.getmtime(file_path)
                if this_file_time > t:
                    try:
                        os.rename(file_path, file_path + '.BACKUP.BACKUP')
                    except Exception as e:
                        a = 'a'
