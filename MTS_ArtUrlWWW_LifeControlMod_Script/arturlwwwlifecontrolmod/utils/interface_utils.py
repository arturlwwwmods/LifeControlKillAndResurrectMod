import services
from clock import ClockSpeedMode
from protocolbuffers.Localization_pb2 import LocalizedString
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender
from sims4.localization import LocalizationHelperTuning, TunableLocalizedStringFactory
from sims4.resources import Types
from ui.ui_dialog import UiDialogOkCancel
from ui.ui_dialog_generic import UiDialogTextInputOkCancel
from ui.ui_dialog_notification import UiDialogNotification
from ui.ui_dialog_picker import ObjectPickerRow, UiObjectPicker

from arturlwwwlifecontrolmod.VersionInfo import get_version, get_release_date
from arturlwwwlifecontrolmod.utils.exceptions_handler import exception_catcher
from arturlwwwlifecontrolmod.utils.string_utils import get_localized_stbl_string, get_localized_string


class ChoiceListPickerRow:
    __qualname__ = 'ChoiceListPickerRow'

    def __init__(self, option_id, name, description, skip_tooltip=False, icon=None, tag=None, tag_itself=False):
        self.option_id = option_id
        self.name = name
        self.description = description
        self.skip_tooltip = skip_tooltip
        self.icon = icon
        self.tag = tag
        self.tag_itself = tag_itself

    def get_option_id(self):
        return self.option_id

    def get_name(self):
        return get_localized_string(self.name)

    def get_description(self):
        return get_localized_string(self.description)

    def get_icon(self):
        return self.icon

    def get_tag(self):
        if self.tag is not None:
            return self.tag
        if self.tag_itself is True:
            return self
        return self.option_id

    def get_object_picker_row(self):
        return ObjectPickerRow(count=0, option_id=self.option_id, name=self.get_name(),
                               row_description=self.get_description(),
                               row_tooltip=None if self.skip_tooltip is True else TunableLocalizedStringFactory._Wrapper(
                                   self.get_description().hash), icon=self.get_icon(), tag=self.get_tag())


@exception_catcher()
def display_choice_list_dialog(title, picker_rows, callback=None):
    client = services.client_manager().get_first_client()
    if client is not None:
        active_sim = client.active_sim
    # else:
    #     active_sim = next(TurboSimUtil.Sim.get_all_sim_instances())
    if active_sim is None:
        return
    if title is None:
        title = get_localized_string('ArtUrlWWW Mega Mod')
    elif title is not LocalizedString:
        title = get_localized_string(title)

    def get_default_callback(dialog):
        if not dialog.accepted:
            return
        result = dialog.get_result_tags()[-1]
        show_notification(text='Missing callback!\nResult: ' + str(result))

    choice_list_dialog = UiObjectPicker.TunableFactory().default(active_sim, title=lambda **_: title, min_selectable=1,
                                                                 max_selectable=1)
    for picker_row in picker_rows:
        if picker_row is None:
            pass
        row = picker_row.get_object_picker_row()
        choice_list_dialog.add_row(row)
    if callback is None:
        choice_list_dialog.add_listener(get_default_callback)
    else:
        choice_list_dialog.add_listener(callback)
    choice_list_dialog.show_dialog()


def get_arrow_icon():
    return services.get_instance_manager(Types.SNIPPET).get(18071476463313889872).icon


@exception_catcher()
def show_notification(text, title=None, sim_info=None):
    client = services.client_manager().get_first_client()
    if sim_info is None:
        sim_info = client.active_sim
    if title is None:
        title = "ArtUrlWWW Life Control Mod"

    if not isinstance(text, LocalizedString):
        text = LocalizationHelperTuning.get_raw_text(text)

    if not isinstance(title, LocalizedString):
        title = LocalizationHelperTuning.get_raw_text(title)

    notification = UiDialogNotification.TunableFactory().default(client.active_sim,
                                                                 text=lambda **_: text,
                                                                 title=lambda **_: title)
    notification.show_dialog(icon_override=(None, sim_info))


@exception_catcher()
def display_question_dialog(text, title=None, callback=None):
    client = services.client_manager().get_first_client()
    if client is not None:
        active_sim = client.active_sim
    # else:
    #     from wickedwoohoo.utils_sim import get_random_active_sim
    #     active_sim = get_random_active_sim()
    if active_sim is None:
        return
    text = get_localized_string(text)
    if title is None:
        title = get_localized_string("ArtUrlWWW Life Control Mod")
    else:
        title = get_localized_string(title)

    def get_default_callback(dialog):
        if not dialog.accepted:
            return

    question_dialog = UiDialogOkCancel.TunableFactory().default(active_sim, text=lambda **_: text,
                                                                title=lambda **_: title)
    if callback is None:
        question_dialog.add_listener(get_default_callback)
    else:
        question_dialog.add_listener(callback)
    question_dialog.show_dialog()


@exception_catcher()
def open_more_informations_dialog():
    def question_callback(dialog):
        if dialog.accepted:
            import webbrowser
            webbrowser.open('http://wickedwhims.tumblr.com')
            services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

    display_question_dialog(get_localized_stbl_string(2528912611), get_localized_stbl_string(3678802915),
                            question_callback)


@exception_catcher()
def pregnancymod_show_scan_results(sim_or_sim_info, title):
    if sim_or_sim_info is None:
        return

    if isinstance(sim_or_sim_info, Sim):
        info = sim_or_sim_info.sim_info  # type: SimInfo
    else:
        info = sim_or_sim_info  # type: SimInfo

    simNameLStr = get_localized_string(object_value=info.first_name + " " + info.last_name)

    pregnancy_tracker = info.pregnancy_tracker
    if pregnancy_tracker.is_pregnant:
        pregnancy_tracker.create_offspring_data()
        num_girls = 0
        num_boys = 0
        for offspring_data in pregnancy_tracker.get_offspring_data_gen():
            if offspring_data.gender == Gender.FEMALE:
                num_girls = num_girls + 1
            else:
                num_boys = num_boys + 1
        pregnancy_tracker._offspring_data = []
        if (num_girls, num_boys) == (1, 0):
            # result = "{} is pregnant, it's a girl!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0xD4D34B71)  # a girl!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (0, 1):
            # result = "{} is pregnant, it's a boy!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0xB7C77BB9)  # a boy!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (2, 0):
            # result = "{} is pregnant, it's twin girls!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0x36E5F148)  # twin girls!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (1, 1):
            # result = "{} is pregnant, it's twins, one girl and one boy!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0x03849AEF)  # twins, one girl and one boy!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (0, 2):
            # result = "{} is pregnant, it's twin boys!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0x59525DCA)  # twin boys!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (3, 0):
            # result = "{} is pregnant, it's triplet girls!!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0x3DF43D41)  # triplet girls!!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (2, 1):
            # result = "{} is pregnant, it's triplets, two girls and a boy!!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0x8BF69760)  # triplets, two girls and a boy!!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (1, 2):
            # result = "{} is pregnant, it's triplets, one girls and a two boys!!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0xD92E2437)  # triplets, one girls and a two boys!!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif (num_girls, num_boys) == (0, 3):
            # result = "{} is pregnant, it's triplet boys!!!".format(simNameLStr)
            resultTmp = get_localized_string(object_value=0xC4BA9537)  # triplet boys!!!
            result = get_localized_string(object_value=0xA392F1E6, tokens=(simNameLStr, resultTmp,), )
        elif num_girls + num_boys == 0:  # should not be reached
            # result = "{} is not pregnant".format(simNameLStr)
            result = get_localized_string(object_value=0x6457D4D8, tokens=(simNameLStr,))
        else:  # should not be reached
            # result = "{} is pregnant with {} girl(s) and {} boy(s)!".format(simNameLStr, num_girls, num_boys)
            result = get_localized_string(object_value=0xC3F9C147, tokens=(simNameLStr, num_girls, num_boys,), )
    else:
        # result = "{} is not pregnant.".format(simNameLStr)
        result = get_localized_string(object_value=0x6457D4D8, tokens=(simNameLStr,))

    show_notification(result, title=title, sim_info=info)


class CustomUiDialogTextInputOkCancel(UiDialogTextInputOkCancel):
    __qualname__ = 'CustomUiDialogTextInputOkCancel'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text_input_responses = {}

    def on_text_input(self, text_input_name='', text_input=''):
        self.text_input_responses[text_input_name] = text_input
        return False

    def build_msg(self, text_input_overrides=None, additional_tokens=(), **kwargs):
        msg = super().build_msg(additional_tokens=(), **kwargs)
        text_input_msg = msg.text_input.add()
        text_input_msg.text_input_name = 'userinput'
        if len(additional_tokens) > 0 and additional_tokens[0] is not None:
            text_input_msg.initial_value = get_localized_string(additional_tokens[0])
        return msg


def display_text_input_dialog(text, title=None, initial_text=None, sim_info=None, callback=None):
    client = services.client_manager().get_first_client()
    active_sim = client.active_sim

    if active_sim is None:
        return
    text = get_localized_string(text)
    if title is None:
        title = get_localized_string('ArtUrlWWW Life Control Mod')
    else:
        title = get_localized_string(title)

    def get_default_callback(dialog):
        if not dialog.accepted:
            return
        userinput = dialog.text_input_responses.get('userinput')
        show_notification(text=str(userinput))

    text_input_dialog = CustomUiDialogTextInputOkCancel.TunableFactory().default(active_sim, text=lambda **_: text,
                                                                                 title=lambda **_: title)
    if callback is None:
        text_input_dialog.add_listener(get_default_callback)
    else:
        text_input_dialog.add_listener(callback)
    if sim_info is None:
        text_input_dialog.show_dialog(additional_tokens=(initial_text,))
    else:
        text_input_dialog.show_dialog(icon_override=(None, sim_info))


@exception_catcher()
def pregnancymod_showmodversion():
    client = services.client_manager().get_first_client()

    mod_version = get_version()
    release_date = get_release_date()
    localized_text = get_localized_string(object_value=0xEB05C9B0,
                                          tokens=(mod_version, release_date),
                                          )

    localized_title = LocalizationHelperTuning.get_raw_text("");

    visual_type = UiDialogNotification.UiDialogNotificationVisualType.INFORMATION
    urgency = UiDialogNotification.UiDialogNotificationUrgency.DEFAULT

    # Prepare and show the notification
    notification = UiDialogNotification.TunableFactory().default(client.active_sim, text=lambda **_: localized_text,
                                                                 title=lambda **_: localized_title,
                                                                 visual_type=visual_type, urgency=urgency)

    notification.show_dialog()


def show_sim_age_up_dialog(sim_info: SimInfo, previous_skills=None, previous_trait_guid=None):
    # from sims.aging.aging_transition import AgingTransition
    # from event_testing.resolver import SingleSimResolver
    # dialog = AgeTransitions.AGE_TRANSITION_INFO[sim_info.age].age_transition_dialog(sim_info,
    #                                                                                 assignment_sim_info=sim_info,
    #                                                                                 resolver=SingleSimResolver(
    #                                                                                     sim_info))
    # AgingTransition.age_transition_dialog
    #
    # age_transition_data.show_age_transition_dialog(self._sim_info, previous_skills=self._previous_skills,
    #                                                previous_trait_guid=self._previous_trait_guid, from_age_up=True,
    #                                                life_skill_trait_ids=self._life_skill_trait_ids)
    # dialog.show_dialog(previous_skills=previous_skills, previous_trait_guid=previous_trait_guid, from_age_up=True)


    age_transition_data = sim_info.get_age_transition_data(sim_info.age)
    # life_skill_trait_ids=sim_info.advance_age()
    life_skill_trait_ids = sim_info._apply_life_skill_traits()
    age_transition_data.show_age_transition_dialog(sim_info, previous_skills=previous_skills,
                                                   previous_trait_guid=previous_trait_guid, from_age_up=True,
                                                   life_skill_trait_ids=life_skill_trait_ids)
