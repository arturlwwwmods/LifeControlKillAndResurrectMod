import os


class MyLogger:
    logger = None  # type: MyLogger

    def __init__(self, fileName, writeMethod="a", fake=False):
        self.fileName = fileName
        self.fake = fake

        if fake != True:
            self.fileObj = open(self._get_ww_file_path(fileName), writeMethod, buffering=1,
                                encoding='utf-8')

    def __del__(self):
        if self.fake != True:
            self.fileObj.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.fake != True:
            self.fileObj.close()

    def _get_ww_file_path(self, fileNameLocal):
        file_path = ''
        root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
        root_file_split = root_file.split(os.sep)
        exit_index = len(root_file_split) - root_file_split.index('Mods')
        for index in range(0, len(root_file_split) - exit_index):
            file_path += str(root_file_split[index]) + os.sep
        file_path += fileNameLocal
        return file_path

    def write(self, strMessage):
        if self.fake != True:
            self.fileObj.write(strMessage)
            self.fileObj.flush()

    def writeLine(self, strMessage):
        if self.fake != True:
            self.fileObj.write(strMessage + "\n")
            self.fileObj.flush()

    def close(self):
        if self.fake != True:
            self.fileObj.close()

    @staticmethod
    def get_main_logger():
        if MyLogger.logger is None:
            MyLogger.logger = MyLogger(fileName="ArtUrlWWW_Life_Control_Log.txt", writeMethod="wt")
        return MyLogger.logger  # type: MyLogger
