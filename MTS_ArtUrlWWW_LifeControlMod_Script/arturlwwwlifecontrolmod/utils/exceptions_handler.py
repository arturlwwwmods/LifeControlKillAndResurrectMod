import os
from datetime import datetime
from functools import wraps
from traceback import format_exc

from arturlwwwlifecontrolmod.utils.MyLogger import MyLogger

log = MyLogger.get_main_logger()


def exception_catcher():
    def catch_exception(exception_function):

        @wraps(exception_function)
        def wrapper(*args, **kwargs):
            try:
                return exception_function(*args, **kwargs)
            except:
                log.writeLine("We have an exception!!!")
                _log_trackback(format_exc())

        return wrapper

    return catch_exception


def _log_trackback(trackback):
    exceptionLogFile = _get_ww_exception_file_path()
    log.writeLine("exceptionLogFile is " + exceptionLogFile)
    log_file = open(exceptionLogFile, 'a', buffering=1, encoding='utf-8')
    log_file.write('{} {}\n'.format(datetime.now().strftime('%x %X'), trackback))
    log_file.flush()


def _get_ww_exception_file_path():
    file_path = ''
    root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
    root_file_split = root_file.split(os.sep)
    exit_index = len(root_file_split) - root_file_split.index('Mods')
    for index in range(0, len(root_file_split) - exit_index):
        file_path += str(root_file_split[index]) + os.sep
    file_path += 'MTS_ArtUrlWWW_LifeControlMod_Exception.txt'
    return file_path
