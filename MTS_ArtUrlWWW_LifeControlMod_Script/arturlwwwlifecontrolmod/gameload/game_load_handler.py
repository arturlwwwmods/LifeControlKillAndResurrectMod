import services
from clock import ClockSpeedMode
from sims.sim import Sim
from sims4.localization import LocalizationHelperTuning

import requests
from arturlwwwlifecontrolmod.MTS_ArtUrlWWW_Life_Control_Instances import MTS_ArtUrlWWW_Life_Control_instance_ids
from arturlwwwlifecontrolmod.VersionInfo import GLOBAL_VERSION, RELEASE_BUILD_NUMBER
from arturlwwwlifecontrolmod.gameload import injector
from arturlwwwlifecontrolmod.gameload.global_manager import register_on_tick_game_function, \
    unregister_on_tick_game_function
from arturlwwwlifecontrolmod.utils.MyLogger import MyLogger
from arturlwwwlifecontrolmod.utils.exceptions_handler import exception_catcher
from arturlwwwlifecontrolmod.utils.interface_utils import display_question_dialog
from arturlwwwlifecontrolmod.utils.interface_utils import show_notification
from arturlwwwlifecontrolmod.utils.string_utils import get_localized_string
from ehp import *

HAS_DISPLAYED_HELLO_MSG = False


@exception_catcher()
@injector.inject_to(Sim, 'on_add')
def pregnancymod_add_super_affordances(original, self):
    original(self)
    sa_list = []

    with MyLogger(fileName="arturlwwwlifecontrolmod_added_affordances.txt", writeMethod="wt") as f:
        f.write("Starting..." + "\n")

        affordance_manager = services.affordance_manager()
        for sa_id in MTS_ArtUrlWWW_Life_Control_instance_ids:
            tuning_class = affordance_manager.get(sa_id)
            f.write("Trying " + str(sa_id) + "\n")
            if not tuning_class is None:
                sa_list.append(tuning_class)
                f.write("Added " + str(sa_id) + "\n")
            else:
                f.write("Can't add " + str(sa_id) + " !!!\n")
        self._super_affordances = self._super_affordances + tuple(sa_list)


@exception_catcher()
class FileLogHandler:
    def __init__(self, name, flags="wt", *args, **kwargs):
        self.file = open(name, flags, *args, **kwargs)

    def write(self, string):
        self.file.write(string)
        self.flush()  # maybe overused but ensures file is updated regularly

    def close(self):
        self.file.close()

    def flush(self):
        self.file.flush()


@exception_catcher()
@register_on_tick_game_function()
def _display_hello_notification_on_tick(ticks):
    global HAS_DISPLAYED_HELLO_MSG
    if HAS_DISPLAYED_HELLO_MSG is False:
        HAS_DISPLAYED_HELLO_MSG = True
        checknewversiononartfunpw()
        # checknewversiononmodthesims()
        unregister_on_tick_game_function('_display_hello_notification_on_tick')


@exception_catcher()
def checknewversiononartfunpw():
    r = requests.get('http://artfun.pw/mods/ArtUrlWWW_LifeControlMod/?v=1')

    if r.status_code == 200:
        modVersion = None
        modVersionDate = None
        html = Html()
        dom = html.feed(r.text)
        for ind in dom.find('span', ('class', 'versionDiv')):
            modVersion = ind.text().strip()

        for ind in dom.find('span', ('class', 'releaseDateDiv')):
            modVersionDate = ind.text().strip()

        if modVersion is not None:
            modVersionNumbers = modVersion.split(".")
            if int(modVersionNumbers[0]) > int(GLOBAL_VERSION):
                _show_need_update_message_universal(modVersion, modVersionDate,
                                                    'http://artfun.pw/mods/ArtUrlWWW_LifeControlMod/')
            else:
                if int(modVersionNumbers[0]) == int(GLOBAL_VERSION):
                    if int(modVersionNumbers[1]) > int(RELEASE_BUILD_NUMBER):
                        _show_need_update_message_universal(modVersion, modVersionDate,
                                                            'http://artfun.pw/mods/ArtUrlWWW_LifeControlMod/')


@exception_catcher()
def _show_need_update_message_universal(modVersion, modVersionDate, urlToOpen):
    localized_title = LocalizationHelperTuning.get_raw_text("!!!MTS_ArtUrlWWW_LifeControlMod!!!");
    localized_text = LocalizationHelperTuning.get_raw_text(
        "A new version " + str(modVersion) + " of ArtUrlWWW Life Control Mod from " + str(
            modVersionDate) + " is available! \n\n"
        + "Do you want to visit ArtUrlWWW Life Control Mod page to download file?\n\nThis will open your web browser!");

    def question_callback(dialog):
        if dialog.accepted:
            import webbrowser
            webbrowser.open(urlToOpen)
            services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

    display_question_dialog(text=localized_text, callback=question_callback)

    show_notification(text=localized_text)

    # text = get_localized_string(object_value=0x82A826BE, tokens=(modVersion, modVersionDate), )
    # display_question_dialog(text=text, callback=question_callback)
    #
    # text = get_localized_string(object_value=0x9B623C87, tokens=(modVersion, modVersionDate), )
    # show_notification(text=text)


@exception_catcher()
def checknewversiononmodthesims():
    r = requests.get('http://www.modthesims.info/download.php?t=600424&v=1')

    if r.status_code == 200:
        modVersion = None
        html = Html()
        dom = html.feed(r.text)
        # dom = html.feed(doc)
        # for ind in dom.find('title'):
        for ind in dom.find('div', ('class', 'pull-left')):
            htmlText = ind.text()
            if htmlText.find("by ArtUrlWWW :") > -1:
                modVersion = htmlText[(htmlText.find("by ArtUrlWWW :") + 14):].strip()

        if modVersion is not None:
            modVersion = modVersion.split(";")
            modVersionNumbers = modVersion[0].split(".")
            if int(modVersionNumbers[0]) > int(GLOBAL_VERSION):
                _show_need_update_message_universal(modVersion, modVersionNumbers,
                                                    'http://www.modthesims.info/download.php?t=600424')
            else:
                if int(modVersionNumbers[0]) == int(GLOBAL_VERSION):
                    if int(modVersionNumbers[1]) > int(RELEASE_BUILD_NUMBER):
                        _show_need_update_message_universal(modVersion, modVersionNumbers,
                                                            'http://www.modthesims.info/download.php?t=600424')