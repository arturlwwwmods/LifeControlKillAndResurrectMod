import traceback

import services
from clock import ClockSpeedMode
from zone import Zone

from arturlwwwlifecontrolmod.gameload import injector
from arturlwwwlifecontrolmod.utils.MyLogger import MyLogger

ON_TICK_FUNCTIONS = {}
UNREGISTER_FUNCTIONS = []

ON_GAME_LOAD_FUNCTIONS = []

LAST_ABSOLUTE_TICKS = 0
IS_FIRST_GAME_LOAD = True
HAS_GAME_LOAD_ON_TICK = False
EXTR_N = 0

log = MyLogger.get_main_logger()


@injector.inject_to(Zone, 'update')
def _pregnancy_mega_mod_zone_game_update(original, self, *args, **kwargs):
    global HAS_GAME_LOAD_ON_TICK, IS_FIRST_GAME_LOAD, LAST_ABSOLUTE_TICKS, EXTR_N
    result = original(self, *args, **kwargs)

    try:
        absolute_ticks = args[0]

        if not self.is_zone_running:
            HAS_GAME_LOAD_ON_TICK = False

        if self.is_zone_running and HAS_GAME_LOAD_ON_TICK is False:
            HAS_GAME_LOAD_ON_TICK = True
            # if IS_FIRST_GAME_LOAD is True:


            for execute_function in ON_GAME_LOAD_FUNCTIONS:
                execute_function(IS_FIRST_GAME_LOAD)
            IS_FIRST_GAME_LOAD = False

        game_clock = services.game_clock_service()

        if game_clock.clock_speed != ClockSpeedMode.PAUSED:
            diff_ticks = (absolute_ticks - LAST_ABSOLUTE_TICKS) * game_clock.current_clock_speed_scale()
            LAST_ABSOLUTE_TICKS = absolute_ticks
            _tick_game(diff_ticks)
            # EXTR_N += diff_ticks
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)

    return result


def register_on_tick_game_function():
    try:
        def regiser_to_collection(method):
            ON_TICK_FUNCTIONS[method.__name__] = method
            return method

        return regiser_to_collection
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)


def unregister_on_tick_game_function(method_name):
    try:
        UNREGISTER_FUNCTIONS.append(method_name)
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)


def _tick_game(ticks):
    try:
        global UNREGISTER_FUNCTIONS
        for unregister_function_name in UNREGISTER_FUNCTIONS:
            del ON_TICK_FUNCTIONS[unregister_function_name]
        UNREGISTER_FUNCTIONS = []
        for (tick_function_name, tick_function) in ON_TICK_FUNCTIONS.items():
            try:
                tick_function(ticks)
            except:
                pass
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)
