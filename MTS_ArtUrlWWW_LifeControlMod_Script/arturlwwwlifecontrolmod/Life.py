import services
import sims4
import sims4.collections
import sims4.collections
import sims4.commands
import traits.traits as traits
from interactions.utils.death import DeathTracker
from objects import ALL_HIDDEN_REASONS
from protocolbuffers import Consts_pb2
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from sims.sim_spawner import SimSpawner
from ui.ui_dialog_picker import SimPickerRow, UiSimPicker

from arturlwwwlifecontrolmod.utils.exceptions_handler import exception_catcher
from arturlwwwlifecontrolmod.utils.interface_utils import show_notification, display_text_input_dialog
from arturlwwwlifecontrolmod.utils.string_utils import get_localized_string


@exception_catcher()
def KillCurrentSim(sim_info_inp=None):
    if sim_info_inp is not None:
        coms = sim_info_inp.commodity_tracker.get_all_commodities()
        hunger = getMotiveByName(coms, "hunger")
        hunger.set_value(hunger.min_value)


def getCommodityName(com):
    """ extracts the name of the commodity """
    return str(com).split('(')[1].split('@')[0]


def getMotiveByName(coms, com_name):
    """ retrieves the appropriate motive """
    com_name = "motive_" + com_name.capitalize()
    for com in coms:
        if getCommodityName(com) == com_name:
            return com
    return None


@exception_catcher()
def revivesim(sim_info_inp=None):
    #######################################
    def get_inputs_callback(dialog):
        if not dialog.accepted:
            return

        if sim_info_inp is None:
            client = services.client_manager().get_first_client()
            active_sim_info = client.active_sim.sim_info  # type: SimInfo
        else:
            active_sim_info = sim_info_inp  # type: SimInfo

        full_name = active_sim_info.first_name + " " + active_sim_info.last_name
        for sim_id in dialog.get_result_tags():
            sim_info_picked = services.sim_info_manager().get(sim_id)  # type: SimInfo

            if sim_info_picked is not None:
                sim_info_picked.trait_tracker.remove_traits_of_type(traits.TraitType.GHOST)
                dt = sim_info_picked.death_tracker  # type: DeathTracker
                dt.clear_death_type()
                sim_info_picked.update_age_callbacks()
                sim = sim_info_picked.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
                if sim is not None:
                    sim.routing_context.ghost_route = False

                household_manager = services.household_manager()
                household_manager.switch_sim_household(sim_info_picked, active_sim_info)

                SimSpawner.load_sim(sim_info_picked.sim_id)

            c = "44444"
            result = get_localized_string(object_value=0xA92F2705, tokens=(c, c), )
            sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)
            show_notification(result, title=c, sim_info=active_sim_info)

            #######################################

    localized_title = lambda **_: get_localized_string(object_value=0x8948B354)  # Select sim
    localized_text = lambda **_: get_localized_string(
        object_value=0x572C0443)  # Revive sim

    max_selectable_immutable = sims4.collections.make_immutable_slots_class(
        set(['multi_select', 'number_selectable', 'max_type']))
    max_selectable = max_selectable_immutable({'multi_select': False, 'number_selectable': 1, 'max_type': 1})

    client = services.client_manager().get_first_client()
    dialog = UiSimPicker.TunableFactory().default(client.active_sim, text=localized_text, title=localized_title,
                                                  max_selectable=max_selectable, min_selectable=1,
                                                  should_show_names=True,
                                                  hide_row_description=False)

    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type: SimInfo
        a = sim_info.death_tracker  # type: DeathTracker
        if a.death_time is not None and a.death_time > 0:
            # Set second arg below to True to have that sim preselected/highlighted....
            dialog.add_row(SimPickerRow(sim_info.sim_id, False, tag=sim_info.sim_id))
    dialog.add_listener(get_inputs_callback)
    dialog.show_dialog(icon_override=(None, sim_info))


def addmoneydialog():
    def add_money_callback(dialog):
        if not dialog.accepted:
            return
        userinput = dialog.text_input_responses.get('userinput')
        show_notification(text=str(userinput))
        sim = services.client_manager().get_first_client().active_sim  # type:Sim

        reason = Consts_pb2.TELEMETRY_MONEY_CHEAT
        modify_fund_helper(int(str(userinput)), reason, sim)

    display_text_input_dialog(text=str(
        'Add money to current sim household (max 10000000). Only numbers! Sum should be without spaces, points or any other symbols!'),
                              title='Add money', initial_text='10000000', callback=add_money_callback)


def modify_fund_helper(amount, reason, sim):
    if amount > 0:
        sim.family_funds.add(amount, reason, sim)
    else:
        sim.family_funds.try_remove(-amount, reason, sim)
