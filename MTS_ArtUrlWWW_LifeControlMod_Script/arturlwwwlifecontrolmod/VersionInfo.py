GLOBAL_VERSION = '2'
RELEASE_BUILD_NUMBER = '1'
RELEASE_DATE = '2017-11-12'


def get_version():
    return GLOBAL_VERSION + '.' + RELEASE_BUILD_NUMBER


def get_release_date():
    return RELEASE_DATE
